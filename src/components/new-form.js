import React , { Component } from "react";
import axios from 'axios';
import '../App.css';
import { Config } from '../config';

const token = sessionStorage.getItem("token");

class NewForm extends Component {

    constructor(props){
        super(props);

        this.state = {
            titleText : '',
            title : '',
            description : '', 
            image : '',
            imagePreviewUrl: '',
            
        }
    }

    componentDidMount(){
        
        let id = this.props.match.params.id;

        if( id !== undefined){
            this.setState({ titleText : 'Edit New'});

            axios.get(Config.apiUrl + 'new/'+id)
            .then(response => {
                
                if(response.data.result){
                    let newItem = response.data.new;
                    this.setState({
                        title : newItem.title,
                        description: newItem.description,
                        imagePreviewUrl: newItem.image
                    });
                }
            })
            .catch( (error) =>{

            })
        }else{
            this.setState({ titleText : 'Add New'});
        }
    }

    onTitleChange(e){
        e.preventDefault();
        this.setState({title : e.target.value})
    }

    onDescriptionChange(e){
        e.preventDefault();
        this.setState({description : e.target.value})
    }
    
    onImageChange(e){
        e.preventDefault();

        let reader = new FileReader();
        let file = e.target.files[0];
        
        reader.onloadend = () => {
            this.setState({
                image: file,
                imagePreviewUrl: reader.result
            });
        };

        reader.readAsDataURL(file);
    }
    
    onUpload(){

        let id = this.props.match.params.id;

        const formData = new FormData();
        formData.append('title', this.state.title);
        formData.append('description', this.state.description);
        formData.append('image',this.state.image);

        const config = {
            headers: {
                'content-type': 'multipart/form-data',
                'authToken' : token
            }
        }

        if( id === undefined){

            axios.post(Config.apiUrl + 'new',formData, config)
            .then(response => {
                
                alert(response.data.message);
            })
        }else{
            axios.post(Config.apiUrl + 'new/' +id,formData, config)
            .then(response => {
                
                alert(response.data.message);
                
            })
            .catch( (error) =>{

            })
        }

        
        axios.get(Config.apiUrl + 'newlist')
            .then(response => {
                
                if(response.data.result){
                    this.setState({newList : response.data.newList});
                }
            })
            .catch( (error) =>{

            })
    }

    render(){

        let imgPreview = <div style={{height: 170}}/>;
        
        if(this.state.imagePreviewUrl){
            imgPreview = <img src={this.state.imagePreviewUrl} alt="preview" style={{marginTop: 10, marginBottom: 10 , maxHeight: 150, maxWidth: 150}}/>
        }

        return(
            <div className="login-form">
                <h2 className="text-center">{ this.state.titleText }</h2>       
                <div className="form-group">
                    <input type="text" 
                        className="form-control" placeholder="title" required="required"
                        onChange={ (e) => this.onTitleChange(e)}
                        value={this.state.title}
                        />
                </div>
                <div className="form-group">
                    <textarea
                        className="form-control" placeholder="description" required="required"
                        style={{ height: 150}}
                        onChange={ (e) => this.onDescriptionChange(e)}
                        value={this.state.description}
                    />
                </div>
                <div>
                    <input type="file" onChange={ (e) => this.onImageChange(e) }/>
                </div>
                <div className="imgPreview">
                    { imgPreview }
                </div>
                <div className="form-group">
                    <button type="submit" className="btn btn-primary btn-block" onClick={ () => this.onUpload() }>
                        Save
                    </button>
                </div>
                    
            </div>
        );
    }
}

export default NewForm;