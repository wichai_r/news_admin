import React , { Component } from "react";
import { BrowserRouter as Router, Redirect, Route } from "react-router-dom";
import "bootstrap/dist/css/bootstrap.min.css";

import Login from './components/login';
import AdminNew from './components/admin-new';
import NewForm from './components/new-form';

class App extends Component {

  constructor(props){
    super(props);
    
    this.state = {
      token : sessionStorage.getItem("token")
    }
  }

  onLoginSuccess = (token) => {
    sessionStorage.setItem("token",token);
    this.setState({ token });
    
  }

  onLogout(){
    sessionStorage.removeItem("token");
    this.setState({ token : '' });
    
  }

  homeRoute(){
    
    if(!this.state.token){
      
      if(window.location.pathname === '/'){
        return(
          <Route path="/" exact >
            <Login onLoginSuccess={ this.onLoginSuccess} />
          </Route>
          
        )
      }else{
        
        return(
          
            <Redirect to="/" />
          
        );
      }

    }else{

      return(
        <div>
          <div style={{justifyContent :'space-between'}}>
            <div style={{display: 'inline-block'}}>
              <div style={{display: 'inline-block' , marginRight: 10 }}>
                <a href="/" className="btn btn-primary btn-block" >
                    Home
                </a>
              </div>
              Admin new console
            </div>
            <div style={{display: 'inline-block' , float : 'right' }}>
              <div style={{display: 'inline-block' , marginRight: 10 }}>
                <a href="/new" className="btn btn-primary btn-block" >
                    Add New
                </a>
              </div>
              <div style={{display: 'inline-block' }}>
                <button type="button" className="btn btn-primary btn-block" 
                  onClick={ () => this.onLogout() }>
                    Log out
                </button>
              </div>
            </div>
          </div>
          <Route path="/" exact component={AdminNew} />
          <Route path="/new/:id?" component={NewForm} />
        </div>
      )
    }
  }

  render(){

    return (
      <Router>
      <div className="container">
        
        { this.homeRoute() }
      </div>
      </Router>
    );

  }
}

export default App;
