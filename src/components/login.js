import React , { Component } from "react";
import axios from 'axios';
import '../App.css';

import { Config } from '../config';

class Login extends Component {

    constructor(props){
        super(props);

        this.state = {
            username : '',
            password : ''
        }
        this.onLogin = this.onLogin.bind(this);
    }

    onUsernameChange(e){
        
        this.setState({username : e.target.value})
    }

    onPasswordChange(e){
        
        this.setState({password : e.target.value})
    }

    onLogin(){

        if(this.state.username !== '' && this.state.password !== ''){
            
            var headers = {
                username: this.state.username,
                password: this.state.password
            };

            axios.post(Config.apiUrl + 'auth', headers)
                .then(response => {
                    
                    if(response.data.result){
                        this.props.onLoginSuccess(response.data.token);
                    }else{
                        alert(response.data.message);
                    }
                    
                })
                .catch( (error) =>{
                    
                })
        }
    }

    render(){
        
        return(
            <div className="login-form">
                <div >
                    <h2 className="text-center">Log in</h2>       
                    <div className="form-group">
                        <input type="text" 
                            className="form-control" placeholder="Username" required="required"
                            onChange={ (e) => this.onUsernameChange(e)}
                            value={this.state.username}
                            />
                    </div>
                    <div className="form-group">
                        <input type="password" 
                            className="form-control" placeholder="Password" required="required"
                            onChange={ (e) => this.onPasswordChange(e)}
                            value={this.state.password}
                        />
                    </div>
                    <div className="form-group">
                        <button type="submit" className="btn btn-primary btn-block" onClick={ () => this.onLogin() }>
                            Log in
                        </button>
                    </div>
                    
                </div>
            </div>
        );
    }
}

export default Login;