import React , { Component } from "react";
import axios from 'axios';
import { Link } from "react-router-dom";
import '../App.css';
import { Config } from '../config';

const token = sessionStorage.getItem("token");

const NewItem = props => (
    <tr >
        
        <td>{props.item.title}</td>
        <td>{props.item.description}</td>
        <td>{props.item.created_date}</td>
        <td>
            <div style={{display: 'inline-block' , marginRight: 10 }}>
                <Link to={"/new/"+props.item.id} 
                    className="btn btn-primary btn-block"  >
                    edit
                </Link>
            </div>
            <div style={{display: 'inline-block' }}>
                <button type="button" className="btn btn-primary btn-block" 
                    onClick={() => { props.onDeleteItem(props.item.id)} }> delete
                </button>
            </div>
        </td>
    </tr>
)

class AdminNew extends Component {

    constructor(props){
        super(props);

        this.state = {
            newList : []
        }
    
    }

    componentDidMount(){
        this.loadNewList();
    }

    loadNewList(){
        axios.get(Config.apiUrl + 'newlist')
        .then(response => {
            
            if(response.data.result){
                this.setState({newList : response.data.newList});
            }
        })
        .catch( (error) =>{

        })
    }

    onDeleteNewItem = (id) => {

        const config = {
            headers: {
                'content-type': 'application/x-www-form-urlencoded',
                'authToken' : token
            }
        }

        axios.delete(Config.apiUrl + 'new/'+id, config )
        .then(response => {
            alert(response.data.message);
            this.loadNewList();
        })
        .catch( (error) =>{

        })
        
    }

    renderNewList() {
        return this.state.newList.map(function(currentItem, i){
            return <NewItem item={currentItem} key={i} onDeleteItem={ this.onDeleteNewItem} />;
        }, this)
    }

    render(){
        return(
            <div>
                <h2 className="text-center">New admin</h2>       
                <table border="1">
                    <thead>
                        <tr>
                            <th style={{width: '20%'}}>Title</th>
                            <th style={{width: '47%'}}>Description</th>
                            <th style={{width: '15%'}}>Create date</th>
                            <th style={{width: '18%'}}>Action</th>
                            
                        </tr>
                    </thead>
                    <tbody>
                        { this.renderNewList() }
                    </tbody>
                </table>
            </div>
        );
    }
}

export default AdminNew;